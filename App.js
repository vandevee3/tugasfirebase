import React, { useEffect } from 'react';
import { View, Button } from 'react-native';
import crashlytics from '@react-native-firebase/crashlytics';
import analytics from '@react-native-firebase/analytics';
import messaging from '@react-native-firebase/messaging';

async function onSignIn(user) {
  crashlytics().log('User signed in.');
  await Promise.all([
    crashlytics().setUserId(user.uid),
    crashlytics().setAttribute('credits', String(user.credits)),
    crashlytics().setAttributes({
      role: 'admin',
      followers: '13',
      email: user.email,
      username: user.username,
    }),
  ]);
}

export default function App() {

  const onSetupCloudMessaging = async () => {
    const authStatus = await messaging().requestPermission();
    const enabled = 
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    if (enabled){
      console.log('Authorizaton status : ', authStatus);
    }
  }

  const onLogScreenView = async () => {
    try {
      await analytics().logScreenView({
        screen_name : 'Home',
        screen_class : 'Home'
      })
    } catch(error){
      console.log(error)
    }
  }

  useEffect(() => {
    crashlytics().log('App mounted.');
    onSetupCloudMessaging();
    onLogScreenView();

    const unsubscribe = messaging().onMessage(async remoteMessage => {
      Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage));
    });
     return unsubscribe;

  }, []);

  return (
    <View>
      <Button
        title="Sign In"
        onPress={() =>
          onSignIn({
            uid: 'Aa0Bb1Cc2Dd3Ee4Ff5Gg6Hh7Ii8Jj9',
            username: 'Joaquin Phoenix',
            email: 'phoenix@example.com',
            credits: 42,
          })
        }
      />
      <Button title="Test Crash" onPress={() => crashlytics().crash()} />
      <Button 
        title='Press Me'

        onPress={async () => await analytics().logSelectContent({
          content_type : 'clothing',
          item_id : 'abcd'
        })}
      />
    </View>
  );
}